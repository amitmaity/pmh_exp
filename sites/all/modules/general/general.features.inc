<?php
/**
 * @file
 * general.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function general_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
}

/**
 * Implements hook_views_api().
 */
function general_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function general_flag_default_flags() {
  $flags = array();
  // Exported flag: "Cover photo".
  $flags['cover_photo'] = array(
    'entity_type' => 'field_collection_item',
    'title' => 'Cover photo',
    'global' => 1,
    'types' => array(
      0 => 'field_images',
    ),
    'flag_short' => 'Assign as cover photo',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unassign as cover photo',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'api_version' => 3,
    'module' => 'general',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Slider".
  $flags['slider'] = array(
    'entity_type' => 'field_collection_item',
    'title' => 'Slider',
    'global' => 0,
    'types' => array(
      0 => 'field_images',
    ),
    'flag_short' => 'Show in Slider',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Remove from Slider',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'api_version' => 3,
    'module' => 'general',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;

}

/**
 * Implements hook_node_info().
 */
function general_node_info() {
  $items = array(
    'blog' => array(
      'name' => t('Blog'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

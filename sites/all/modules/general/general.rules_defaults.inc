<?php
/**
 * @file
 * general.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function general_default_rules_configuration() {
  $items = array();
  $items['rules_limit_cover_photo'] = entity_import('rules_config', '{ "rules_limit_cover_photo" : {
      "LABEL" : "Limit Cover Photo",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "flag", "flag_limit", "field_collection" ],
      "ON" : { "flag_flagged_cover_photo" : [], "field_collection_item_update" : [] },
      "DO" : [
        { "flag_fetch_field_collection_item_by_user" : {
            "USING" : { "flag" : "cover_photo", "flagging_user" : [ "flagging_user" ] },
            "PROVIDE" : { "content_flagged_by_user" : { "content_flagged_by_user" : "Content flagged by user" } }
          }
        },
        { "flag_limit_rules_action_mark_cover" : { "field_id" : [ "flagged-field-collection-item:item-id" ] } }
      ]
    }
  }');
  return $items;
}

<?php
function flag_limit_rules_action_info(){
	return array(
    'flag_limit_rules_action_mark_cover' => array(
      'label' => t('Mark as cover'),
      'group' => t('Custom'),
      'parameter' => array(
        'field_id' => array(
          'type' => 'text',
          'label' => t('Flagged Field collection ID'),
          'description' => t('The field collection flagged by user'),
        ),
      ),
    ),
  );
}

function flag_limit_rules_action_mark_cover($field_id){
$query = new EntityFieldQuery();

$query->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'blog')
  ->fieldCondition('field_images', 'value', $field_id , '=');

$result = $query->execute();
$result = array_shift($result['node']);
$nid = $result->nid;
$node = entity_load('node', array($nid));
$node = array_shift($node);
    $flag = flag_get_flag('cover_photo');
    $collection_ids= array();
foreach ($node->field_images['und'] as $key => $value) {
    $collection_ids[]=$value['value'];
    if ($flag) {
        $flag->flag('unflag', $value['value']);
}

}
$flag->flag('flag', $field_id);
}